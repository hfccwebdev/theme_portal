jQuery(document).ready(function($){

  $('.dashboard-tabs button').first().addClass('current');
  $('.dashboard-tabs button').first().attr('aria-selected','true');
  $('.region-dashboard-r-content > .block').first().addClass('current');
  $('.region-dashboard-r-content > .block').attr('hidden');
  $('.region-dashboard-r-content > .block').first().removeAttr('hidden');

  $('.dashboard-tabs button').click(function(){
    var tab_id = $(this).attr('id');
    var block_id = $('.region-dashboard-r-content > .block').attr('id');
    var button_id = $(this).attr('aria-controls');
    var current_block = $('.region-dashboard-r-content > .block').find('[id="'+button_id+'"]');

    // console.log(button_id);
    // console.log(block_id);
    // console.log(current_block);

    $('.dashboard-tabs button').removeClass('current');
    $('.dashboard-tabs button').attr('aria-selected','false');
    $('.region-dashboard-r-content').children().removeClass('current');

    $(this).addClass('current');
    $(this).attr ('aria-selected', 'true');
    $("#"+tab_id).addClass('current');

    if (($("#"+tab_id).hasClass('current'))){
      $('.region-dashboard-r-content > .block').attr('hidden');
       $('.region-dashboard-r-content > .block[id="'+button_id+'"]').addClass('current');
    }
  })

  $('#button-profile-info-toggle').click(function(){
    $('.myhank-profile-info').toggleClass('show-profile-info hide-profile-info');

    if ($('#button-profile-info-toggle').hasClass('show-profile')) {
      $('.view-profile-information-button').addClass('expand-plus');
      $('#button-profile-info-toggle').removeClass('show-profile');
      $('.view-profile-information-button').removeClass('collapse-minus');
      $('.profile-button-text').text('Show Profile Information');
    }

    else {
      $('.view-profile-information-button').removeClass('expand-plus');
      $('.view-profile-information-button').addClass('collapse-minus');
      $('#button-profile-info-toggle').addClass('show-profile');
      $('.profile-button-text').text('Hide Profile Information');
    }

  })

})
