jQuery(document).ready(function($) {

  if ($('.node-webform')||('.entity-entityform-type').length) {

    $('input:not(input[type=checkbox],input[type=radio],input[type=submit],input[type=select])').each(function (i){
      var inputText = $(this).val();
      var placeholder = $(this).attr('placeholder');
      var siblingLabel = $(this).siblings('label');

      if (!(inputText == "" || inputText == null)) {
        $(siblingLabel).addClass('hfc-float-label');
      }

      if (!(placeholder == "" || placeholder == null)) {
        $(siblingLabel).addClass('hfc-float-label');
      }
    })

    var positionLabel = $('input:not(input[type=checkbox],input[type=radio],input[type=submit],input[type=select])').siblings('label')
    $(positionLabel).addClass('hfc-float-position');

    var placeholder = $('input:not(input[type=checkbox],input[type=radio],input[type=submit],input[type=select])').attr('placeholder');

    if ($('input:not(input[type=checkbox],input[type=radio],input[type=submit],input[type=select])').attr('placeholder')){
      var siblingLabel = $(this).siblings('label');
      $(siblingLabel).addClass('hfc-float-label');
    }

    $('input:not(input[type=checkbox],input[type=radio],input[type=submit],input[type=select])').focus(function(){
      var siblingLabel = $(this).siblings('label');
      $(siblingLabel).addClass('hfc-float-label');
    })

    $('input:not(input[type=checkbox],input[type=radio],input[type=submit],input[type=select])').blur(function(){
      var inputText = $(this).val();
      var siblingLabel = $(this).siblings('label');

      if ((inputText === "" || inputText === null) && ($(this).attr('placeholder') === "" ||  ($(this).attr('placeholder') === null))) {
        $(siblingLabel).removeClass('hfc-float-label');
      }
      else {
        $(siblingLabel).addClass('hfc-float-label');
      }
    })
  }

})
