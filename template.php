<?php

/**
 * @file
 * This file provides theme override functions for the portal theme.
 */

/**
 * Implements hook_template_preprocess_html().
 *
 * Preprocess variables for html.tpl.php.
 */
function portal_preprocess_html(&$variables) {
  // Add Google Font.
  drupal_add_css('//fonts.googleapis.com/css?family=Work+Sans:400,500,600,700" rel="stylesheet', array('group' => CSS_THEME, 'type' => 'external'));

  // Force template suggestion for success dashboard iframe.
  if ($matches = preg_grep('|^html__faculty_and_staff__support__.*$|', $variables['theme_hook_suggestions'])) {
    $variables['theme_hook_suggestions'][] = 'html__faculty_and_staff__support__%';
  }
}

/**
 * Implements hook_template_preprocess_page().
 *
 * Preprocess variables for page.tpl.php.
 */
function portal_preprocess_page(&$variables) {
  // Force template suggestion for success dashboard iframe.
  if ($matches = preg_grep('|^page__faculty_and_staff__support__.*$|', $variables['theme_hook_suggestions'])) {
    $variables['theme_hook_suggestions'][] = 'page__faculty_and_staff__support__%';
  }
}

/**
 * Implements hook_template_preprocess_block().
 */
function portal_preprocess_block(&$variables) {
  if ($variables['block']->region == 'dashboard_r_content') {
    $variables['block']->tabsubject = $variables['block']->subject;
    $variables['block']->subject = NULL;
    $variables['attributes_array']['role'] = 'tabpanel';
  }
}

/**
 * Implements hook_template_preprocess_region().
 *
 * @see https://www.w3.org/TR/2017/NOTE-wai-aria-practices-1.1-20171214/examples/tabs/tabs-1/tabs.html
 */
function portal_preprocess_region(&$variables) {
  if ($variables['region'] == 'dashboard_r_content') {

    // Builds a set of buttons to control display of the blocks within this region.
    // The buttons will be prepended to the current region content, in their own wrapper divs.

    $buttons = [];
    foreach (element_children($variables['elements']) as $key) {
      $element = $variables['elements'][$key];
      $block = $element['#block'];
      $delta = $block->delta;
      $target_id = str_replace('_', '-', "block-{$block->module}-{$delta}");
      if (!empty($block->tabsubject)) {
        $subject = $block->tabsubject;
      }
      elseif (!empty($block->subject)) {
        $subject = $block->subject;
      }
      else {
        $subject = 'Untitled Block';
      }
      $buttons[] = t('<button role="tab" aria-selected="!selected" aria-controls="!block" id="!delta">@subject</button>', [
        '!selected' => count($buttons) ? "false" : "true",
        '!block' => $target_id,
        '!delta' => drupal_html_id("button-{$block->module}-{$delta}"),
        '@subject' => $subject,
      ]);
    }
    if (!empty($buttons)) {
      $content = '<div class="dashboard-tabs"><div role="tablist" aria-label="My Dashboard">';
      $content .= implode(' ', $buttons);
      $content .= '</div></div>';
      $variables['content'] = $content . $variables['content'];
    }
  }
}

/**
 * Implements hook_template_preprocess_entity().
 */
function portal_preprocess_entity(&$variables) {

  if ($variables['entity_type'] == 'paragraphs_item') {
    $entity = $variables['elements']['#entity'];
    if (!empty($entity->field_para_classes)) {
      $classes = explode(' ', $entity->field_para_classes[LANGUAGE_NONE][0]['value']);
      foreach ($classes as $class) {
        $variables['classes_array'][] = drupal_html_class($class);
      }
    }
  }
}

/**
 * Implements hook_template_preprocess_username().
 *
 * @see realname_update()
 */
function portal_preprocess_username(&$variables) {

  if ($pattern = theme_get_setting('portal_realname_pattern')) {
    // $account = $variables['account'];
    $account = user_load($variables['account']->uid);
    // dpm($account, 'account');

    // Perform token replacement on the real name pattern.
    $realname = token_replace($pattern, array('user' => $account), array('clear' => TRUE, 'sanitize' => FALSE));

    // Remove any HTML tags.
    $realname = strip_tags(decode_entities($realname));

    // Remove double spaces (if a token had no value).
    $realname = preg_replace('/ {2,}/', ' ', $realname);

    // Trim the name to 255 characters.
    $realname = truncate_utf8(trim($realname), 255);

    // Substitute the real name if it is not empty, otherwise, leave the value unchanged.
    $variables['name'] = !empty($realname) ? $realname : $variables['name'];
  }
}

/**
 * Implements hook_template_preprocess_field().
 */
function portal_preprocess_field(&$variables, $hook) {

  $unwrapped = theme_get_setting('portal_unwrapped');
  if (!empty($unwrapped) && in_array($variables['element']['#field_name'], $unwrapped)) {
    $variables['theme_hook_suggestions'][] = 'field__unwrapped';
  }
}
