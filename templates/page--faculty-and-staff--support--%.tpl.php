    <div id="main-content" role="main">
        <div class="container">
            <?php if ($messages):?>
                <?php print $messages; ?>
            <?php endif; ?>
            <?php print render($page['content']); ?>
        </div>
    </div>
